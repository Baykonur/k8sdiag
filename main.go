// Most of the code is based on from the book: The Go Programming Language (https://www.gopl.io/)

package main

import (
	"context"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
	"time"
)

var (
	serverStart time.Time
	readyCheck  time.Time
	mu          sync.Mutex
	count       int
	port        = flag.String("port", "7999", "port which the Server listens to")
	ctxRoot     = flag.String("ctx-root", "k8sdiag", "context-root of the application")
	readTimeout = flag.Int("readTimeout", 2, "read timeout value (s) for the server")
	writeTimeout = flag.Int("writeTimeout", 4, "write timeout value (s) for the server")
	idleTimeout = flag.Int("idleTimeout", 5, "idle timeout value (s) for the server")
)

// Creates an HTTP server with a counter which is incremented each time main_handler is invoked;
// prints out the request headers and some other related information.
// `/count` prints out the counter.
// The server also implements readiness and liveness end-points for k8s with server up time and the most recent liveness check.
// The main process has 2 flags: port (defaults to 7999), ctx-root (defaults to /k8sdiag)
// The Docker container for this simple program is based on alpine plus curl.

func main() {
	const ip = "0.0.0.0"

	log.SetFlags(log.LstdFlags)
	log.SetPrefix("k8sdiag: ")
	flag.Parse()
	if len(flag.Args()) > 0 {
		flag.PrintDefaults()
		log.Fatal("FATAL ERROR: surplus arguments")
	}

	log.SetOutput(os.Stdout)

	quit := make(chan os.Signal, 1)
	signal.Notify(quit)

	handler := http.NewServeMux()
	server := http.Server{
		Addr:    "0.0.0.0:" + *port,
		Handler: handler,
		ReadTimeout:  time.Duration(*readTimeout) * time.Second,
		WriteTimeout: time.Duration(*writeTimeout) * time.Second,
		IdleTimeout:  time.Duration(*idleTimeout) * time.Second,
	}

	handler.HandleFunc("/check_ready", readyHandler)
	handler.HandleFunc("/check_alive", aliveHandler)
	handler.HandleFunc("/"+*ctxRoot+"/", mainHandler)
	handler.HandleFunc("/"+*ctxRoot+"/count", countHandler)
	handler.HandleFunc("/"+*ctxRoot+"/env", envHandler)
	serverStart = time.Now()
	readyCheck = time.Now()
	log.Printf("Checking the persisted count first: count value starts at %d", readPersistedCount())
	log.Printf("Server Configuration: read-timeout: %v, write-timeout: %v, idle-timeout: %v", server.ReadTimeout, server.WriteTimeout, server.IdleTimeout)
	log.Printf("Server is starting %v:%v. ctx-root is /%s", ip, *port, *ctxRoot)

	idleConnectionsClosed := make(chan struct{})
	go func() {
		shutTheServer := make(chan os.Signal, 1)
		signal.Notify(shutTheServer, syscall.SIGTERM)
		log.Printf("Signal %v received, will try to gracefully shutdown the server in 2 seconds", <-shutTheServer)
		// We received SIGTERM, shut down, but first wait for few seconds (for k8s endpoint removal) and then write the count to the file
		time.Sleep(2*time.Second)
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()
		server.SetKeepAlivesEnabled(false)
		writeCount()
		if err := server.Shutdown(ctx); err != nil {
			log.Fatalf("Could not do a graceful shutdown: %v", err)
		}
		close(idleConnectionsClosed)
	}()
	if err := server.ListenAndServe(); err != http.ErrServerClosed {
		log.Fatalf("HTTP server ListenAndServe: %v", err)
	}
	<-idleConnectionsClosed
}

func readyHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "k8diag readiness check OK on %v, request received from %v\nLast check was %v ago\n", r.Host, r.RemoteAddr, time.Since(readyCheck).Round(time.Second))
	readyCheck = time.Now()
}

func aliveHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "k8diag liveness check OK on %v, request received from %v\nServer is up for %v\n", r.Host, r.RemoteAddr, time.Since(serverStart).Round(time.Second))
}

func envHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "ENVIRONMENTAL VARIABLES")

	for _, v := range os.Environ() {
		fmt.Fprintf(w, "%s\n", v)
	}
}

func mainHandler(w http.ResponseWriter, r *http.Request) {
	// print request headers and some other related stuff here
	fmt.Fprintf(w, "%s %s %s\n", r.Method, r.URL, r.Proto)
	for k, v := range r.Header {
		fmt.Fprintf(w, "Header[%q] = %q\n", k, v)
	}
	fmt.Fprintf(w, "Host = %q\n", r.Host)
	fmt.Fprintf(w, "RemoteAddr = %q\n", r.RemoteAddr)
	if err := r.ParseForm(); err != nil {
		log.Print(err)
	}
	for k, v := range r.Form {
		fmt.Fprintf(w, "Form[%q] = %q\n", k, v)
	}
	// increase the counter
	mu.Lock()
	defer mu.Unlock()
	count++
}

func countHandler(w http.ResponseWriter, r *http.Request) {
	// print the counter
	mu.Lock()
	defer mu.Unlock()
	fmt.Fprintf(w, "Count %d\n", count)
}

func readPersistedCount() int {
	if content, err := ioutil.ReadFile("/data/count.txt"); err == nil {
		if temp, err := strconv.Atoi(string(content)); err == nil {
			count = temp
		} else {
			log.Printf("Byte to Integer conversion could not be achieved, error: %v", err)
		}
	} else {
		log.Printf("Count value cannot be read from its persisted state, error: %v", err)
	}
	return count
}
func writeCount() {
	if err := ioutil.WriteFile("/data/count.txt", []byte(strconv.Itoa(count)), 0644); err != nil {
		log.Printf("Cannot persist count:%d, error: %v", count, err)
	}
}
