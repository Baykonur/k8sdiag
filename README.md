# k8sdiag
HTTP server with a counter which is incremented each time main_handler is invoked; prints out the request headers and some other related information,
<ctx-root>/env handles prints out the environmental variables of the host.

## Build
Use ```-f deploy/docker/Dockerfile``` from the project folder i.e.
```docker build -t baykonur/k8sdiag:<VERSION> -f deploy/docker/Dockerfile .```

## Run (as a Docker container)
```docker run --name k8sdiag -d -p 7999:7999 baykonur/k8sdiag:<VERSION>```
